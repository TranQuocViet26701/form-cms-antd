/* eslint-disable */
// 该文件由 OneAPI 自动生成，请勿手动修改！

declare namespace API {
  interface PageInfo {
    /** 
1 */
    current?: number;
    pageSize?: number;
    total?: number;
    list?: Array<Record<string, any>>;
  }

  interface PageInfo_UserInfo_ {
    /** 
1 */
    current?: number;
    pageSize?: number;
    total?: number;
    list?: Array<UserInfo>;
  }

  interface Result {
    success?: boolean;
    errorMessage?: string;
    data?: Record<string, any>;
  }

  interface Result_PageInfo_UserInfo__ {
    success?: boolean;
    errorMessage?: string;
    data?: PageInfo_UserInfo_;
  }

  interface Result_UserInfo_ {
    success?: boolean;
    errorMessage?: string;
    data?: UserInfo;
  }

  interface Result_string_ {
    success?: boolean;
    errorMessage?: string;
    data?: string;
  }

  type UserGenderEnum = 'MALE' | 'FEMALE';

  interface UserInfo {
    id?: string;
    name?: string;
    /** nick */
    nickName?: string;
    /** email */
    email?: string;
    gender?: UserGenderEnum;
  }

  interface UserInfoVO {
    name?: string;
    /** nick */
    nickName?: string;
    /** email */
    email?: string;
  }

  type TypeFormatEnum =
    | 'STRING'
    | 'DECIMAL'
    | 'NUMBER'
    | 'BOOLEAN'
    | 'DROPDOWN'
    | 'SELECTOR'
    | 'MULTI_SELECTOR'
    | 'FILE'
    | 'DATE'
    | 'DATE_TIME';

  interface SelectValue {
    name: string;
    description?: string;
    icon?: string;
  }

  interface FormField {
    id: string;
    name: string;
    description: string;
    icon: string;
    workSpaceId: string;
    defaultValue: string;
    maxValue: number;
    minValue: number;
    editable: true;
    showUI: true;
    lines: number;
    regEx: string;
    maxLength: number;
    minLength: number;
    required: true;
    replaceCharacter: string;
    placeHolder: string;
    typeFormat: string;
    listSelectValues: Partial<SelectValue>[];
  }

  type definitions_0 = null;
}
