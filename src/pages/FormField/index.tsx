import services from '@/services/demo';
import {
  ActionType,
  FooterToolbar,
  PageContainer,
  ProDescriptions,
  ProDescriptionsItemProps,
  ProTable,
} from '@ant-design/pro-components';
import { Button, Drawer, message, Tag } from 'antd';
import React, { useRef, useState } from 'react';
import CreateForm from './components/CreateForm';
import ProFormField from './components/ProFormField';
import UpdateForm, { FormValueType } from './components/UpdateForm';

const { addUser, queryUserList, deleteUser, modifyUser } =
  services.UserController;

/**
 * 添加节点
 * @param fields
 */
const handleAdd = async (fields: API.UserInfo) => {
  console.log('fields: ', fields);

  const hide = message.loading('Adding');
  try {
    await addUser({ ...fields });
    hide();
    message.success('Added successfully');
    return true;
  } catch (error) {
    hide();
    message.error('Failed to add, please try again！');
    return false;
  }
};

/**
 * 更新节点
 * @param fields
 */
const handleUpdate = async (fields: FormValueType) => {
  const hide = message.loading('Updating');
  try {
    await modifyUser(
      {
        userId: fields.id || '',
      },
      {
        name: fields.name || '',
        nickName: fields.nickName || '',
        email: fields.email || '',
      },
    );
    hide();

    message.success('配置成功');
    return true;
  } catch (error) {
    hide();
    message.error('配置失败请重试！');
    return false;
  }
};

/**
 *  删除节点
 * @param selectedRows
 */
const handleRemove = async (selectedRows: API.UserInfo[]) => {
  const hide = message.loading('正在删除');
  if (!selectedRows) return true;
  try {
    await deleteUser({
      userId: selectedRows.find((row) => row.id)?.id || '',
    });
    hide();
    message.success('删除成功，即将刷新');
    return true;
  } catch (error) {
    hide();
    message.error('删除失败，请重试');
    return false;
  }
};

const valueEnum = {
  STRING: { text: 'STRING' },
  DECIMAL: { text: 'DECIMAL' },
  NUMBER: { text: 'NUMBER' },
  BOOLEAN: { text: 'BOOLEAN' },
  DROPDOWN: { text: 'DROPDOWN' },
  SELECTOR: { text: 'SELECTOR' },
  MULTI_SELECTOR: { text: 'MULTI_SELECTOR' },
  FILE: { text: 'FILE' },
  DATE: { text: 'DATE' },
  DATE_TIME: { text: 'DATE_TIME' },
};

const booleanValueEnum = {
  true: { text: 'true' },
  false: { text: 'false' },
};

const FormFieldList: React.FC<unknown> = () => {
  const [createModalVisible, handleModalVisible] = useState<boolean>(false);
  const [updateModalVisible, handleUpdateModalVisible] =
    useState<boolean>(false);
  const [stepFormValues, setStepFormValues] = useState({});
  const actionRef = useRef<ActionType>();
  const [row, setRow] = useState<API.FormField>();
  const [selectedRowsState, setSelectedRows] = useState<API.FormField[]>([]);
  const columns: ProDescriptionsItemProps<API.FormField>[] = [
    {
      title: 'name',
      dataIndex: 'name',
      tip: 'name',
      formItemProps: {
        rules: [
          {
            required: true,
            message: 'name is required',
          },
        ],
      },
    },
    {
      title: 'description',
      dataIndex: 'description',
      valueType: 'text',
    },
    {
      title: 'icon',
      dataIndex: 'icon',
      valueType: 'text',
    },
    {
      title: 'workSpaceId',
      dataIndex: 'workSpaceId',
      valueType: 'text',
    },
    {
      title: 'defaultValue',
      dataIndex: 'defaultValue',
      valueType: 'text',
    },
    {
      title: 'editable',
      dataIndex: 'editable',
      valueType: 'select',
      render: (_, record) => (
        <Tag color={record ? 'green' : 'red'}>{record ? 'true' : 'false'}</Tag>
      ),
      valueEnum: booleanValueEnum,
    },
    {
      title: 'showUI',
      dataIndex: 'showUI',
      valueType: 'select',
      render: (_, record) => (
        <Tag color={record ? 'green' : 'red'}>{record ? 'true' : 'false'}</Tag>
      ),
      valueEnum: booleanValueEnum,
    },
    {
      title: 'lines',
      dataIndex: 'lines',
      valueType: 'digit',
    },
    {
      title: 'required',
      dataIndex: 'required',
      valueType: 'select',
      render: (_, record) => (
        <Tag color={record ? 'green' : 'red'}>{record ? 'true' : 'false'}</Tag>
      ),
      valueEnum: booleanValueEnum,
    },
    {
      title: 'placeHolder',
      dataIndex: 'placeHolder',
      valueType: 'text',
    },
    {
      title: 'typeFormat',
      dataIndex: 'typeFormat',
      valueType: 'select',
      valueEnum,
    },
    {
      title: 'Operate',
      dataIndex: 'option',
      valueType: 'option',
      render: (_, record) => (
        <>
          <a
            onClick={() => {
              handleUpdateModalVisible(true);
              setStepFormValues(record);
            }}
          >
            配置
          </a>
        </>
      ),
    },
  ];

  return (
    <PageContainer
      header={{
        title: 'Form Field List',
      }}
    >
      <ProTable<API.FormField>
        headerTitle="Form Field Table"
        actionRef={actionRef}
        rowKey="id"
        search={{
          labelWidth: 120,
        }}
        toolBarRender={() => [
          <Button
            key="1"
            type="primary"
            onClick={() => handleModalVisible(true)}
          >
            New
          </Button>,
        ]}
        request={async (params, sorter, filter) => {
          const { data, success } = await queryUserList({
            ...params,
            // FIXME: remove @ts-ignore
            // @ts-ignore
            sorter,
            filter,
          });

          console.log('data: ', data);

          return {
            data: data?.list || [],
            success,
          };
        }}
        columns={columns}
        rowSelection={{
          onChange: (_, selectedRows) => setSelectedRows(selectedRows),
        }}
        scroll={{ x: 1300 }}
      />
      {selectedRowsState?.length > 0 && (
        <FooterToolbar
          extra={
            <div>
              已选择{' '}
              <a style={{ fontWeight: 600 }}>{selectedRowsState.length}</a>{' '}
              项&nbsp;&nbsp;
            </div>
          }
        >
          <Button
            onClick={async () => {
              await handleRemove(selectedRowsState);
              setSelectedRows([]);
              actionRef.current?.reloadAndRest?.();
            }}
          >
            批量删除
          </Button>
          <Button type="primary">批量审批</Button>
        </FooterToolbar>
      )}
      <CreateForm
        onCancel={() => handleModalVisible(false)}
        modalVisible={createModalVisible}
      >
        <ProFormField
          onClick={async (value) => {
            const success = await handleAdd(value);
            if (success) {
              handleModalVisible(false);
              if (actionRef.current) {
                actionRef.current.reload();
              }
            }
          }}
        />
      </CreateForm>
      {stepFormValues && Object.keys(stepFormValues).length ? (
        <UpdateForm
          onSubmit={async (value) => {
            const success = await handleUpdate(value);
            if (success) {
              handleUpdateModalVisible(false);
              setStepFormValues({});
              if (actionRef.current) {
                actionRef.current.reload();
              }
            }
          }}
          onCancel={() => {
            handleUpdateModalVisible(false);
            setStepFormValues({});
          }}
          updateModalVisible={updateModalVisible}
          values={stepFormValues}
        />
      ) : null}

      <Drawer
        width={600}
        visible={!!row}
        onClose={() => {
          setRow(undefined);
        }}
        closable={false}
      >
        {row?.name && (
          <ProDescriptions<API.FormField>
            column={2}
            title={row?.name}
            request={async () => ({
              data: row || {},
            })}
            params={{
              id: row?.name,
            }}
            columns={columns}
          />
        )}
      </Drawer>
    </PageContainer>
  );
};

export default FormFieldList;
