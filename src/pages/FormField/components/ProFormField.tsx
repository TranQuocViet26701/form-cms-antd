import {
  ProForm,
  ProFormDigit,
  ProFormDigitRange,
  ProFormSelect,
  ProFormSwitch,
  ProFormText,
} from '@ant-design/pro-components';
import { Switch } from 'antd';
import { useState } from 'react';

const typeFormatEnum = {
  string: 'STRING',
  decimal: 'DECIMAL',
  number: 'NUMBER',
  boolean: 'BOOLEAN',
  dropdown: 'DROPDOWN',
  selector: 'SELECTOR',
  multiSelector: 'MULTI_SELECTOR',
  file: 'FILE',
  date: 'DATE',
  dateTime: 'DATE_TIME',
};

interface ProFormFieldProps {
  onClick: (value: any) => void;
}

const ProFormField = ({ onClick }: ProFormFieldProps) => {
  const [readonly, setReadonly] = useState(false);
  return (
    <div
      style={{
        padding: 24,
      }}
    >
      <Switch
        style={{
          marginBottom: 16,
        }}
        checked={readonly}
        checkedChildren="Edit"
        unCheckedChildren="Read only"
        onChange={setReadonly}
      />
      <ProForm
        readonly={readonly}
        name="validate_other"
        initialValues={{
          name: '',
          description: '',
          icon: '',
          typeFormat: 'string',
          defaultValue: '',
          rangeValue: [],
          rangeLength: [],
          lines: 1,
          regEx: '',
          replaceCharacter: '',
          placeHolder: '',
          editable: true,
          showUi: true,
          required: true,
        }}
        onValuesChange={(_, values) => {
          console.log(values);
        }}
        onFinish={async (value) => onClick(value)}
      >
        <ProFormText
          name="name"
          label="Name"
          rules={[{ required: true, message: 'Name must be required!' }]}
        />
        <ProFormText name="description" label="Description" />
        <ProFormText name="icon" label="Icon" />
        <ProFormSelect
          name="typeFormat"
          label="Type format"
          valueEnum={typeFormatEnum}
          placeholder="Please select a type format"
          rules={[{ required: true, message: 'Type format must be required!' }]}
        />
        <ProFormText name="defaultValue" label="Default value" />
        <ProFormDigitRange
          label="Range Value"
          name="rangeValue"
          separator=" - "
          separatorWidth={60}
          placeholder={['Min value', 'Max value']}
        />
        <ProFormDigitRange
          label="Range Length"
          name="rangeLength"
          separator=" - "
          separatorWidth={60}
          placeholder={['Min length', 'Max length']}
        />
        <ProFormDigit label="Lines" name="lines" width="sm" min={1} max={10} />
        <ProFormText name="regEx" label="RegEx" />
        <ProFormText name="replaceCharacter" label="Replace character" />
        <ProFormText name="placeHolder" label="Place Holder" />
        <ProFormSwitch name="editable" label="Editable" />
        <ProFormSwitch name="showUi" label="Show UI" />
        <ProFormSwitch name="required" label="Required" />
      </ProForm>
    </div>
  );
};

export default ProFormField;
