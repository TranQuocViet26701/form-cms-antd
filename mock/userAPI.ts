const users = [
  { name: 'Umi', nickName: 'U', gender: 'MALE' },
  { name: 'Fish', nickName: 'B', gender: 'FEMALE' },
];

const formFieldList = [
  {
    name: 'name',
    description: 'desc',
    icon: 'url',
    workSpaceId: '3fa85f64-5717-4562-b3fc-2c963f66afa6',
    defaultValue: 'tran quoc viet',
    editable: true,
    showUI: true,
    lines: 1,
    required: true,
    placeHolder: 'your name',
    typeFormat: 'STRING',
  },
];

export default {
  'GET /api/v1/queryUserList': (req: any, res: any) => {
    res.json({
      success: true,
      data: { list: users },
      errorCode: 0,
    });
  },
  'PUT /api/v1/user/': (req: any, res: any) => {
    res.json({
      success: true,
      errorCode: 0,
    });
  },
  'POST /api/v1/user/': (req: any, res: any) => {
    console.log('req: ', req.body);

    users.push(req.body);

    res.json({
      success: true,
      data: { list: users },
      errorCode: 0,
    });
  },
  'GET /api/v1/queryFormFieldList': (req: any, res: any) => {
    res.json({
      success: true,
      data: { list: formFieldList },
      errorCode: 0,
    });
  },
};
